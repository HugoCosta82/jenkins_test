
using SingaporeSMS.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using SingaporeSMS.Code;
using SingaporeSMS.ViewModels.Common;

namespace SingaporeSMS.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class BalanceViewModel : BaseViewModel
    {
        public BalanceViewModel(IConnectivityService connectivityService, IUiHelper uiHelper, INavigationService navigationService, ILogger logger) : base(connectivityService, uiHelper, navigationService, logger)
        {
        }

        //private GitHubGuy _isFormOk;
        //public bool IsFormOk
        //{
        //  get => _isFormOk;
        //  set
        //  {
        //      if (_isFormOk == value) return;
        //      _isFormOk = value;
        //      NotifyPropertyChanged();

        //  }
        //}

        private string _balance;
        public string Balance
        {
            get => _balance;
            set
            {
                if (_balance == value) return;
                _balance = value;
                NotifyPropertyChanged();
            }
        }

        private bool _navigating;
        public bool Navigating
        {
            get {
                return _navigating;
            }
            set {
                _navigating = value;
            }
        }

        private RelayCommand _goToReceiveMoney;
        public RelayCommand GoToReceiveMoney
        {
            get
            {
                return _goToReceiveMoney ?? (_goToReceiveMoney = new RelayCommand(
            async () =>
            {
                try
                {
                    if (_navigating) return;
                    _navigating = true;
                        await this.NavigationService.NavigateToReceiveMoney();
                    _navigating = false; 

                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {

                }
            }));
            }
        }

        private RelayCommand _goToSendMoney;
        public RelayCommand GoToSendMoney
        {
            get
            {
                return _goToSendMoney ?? (_goToSendMoney = new RelayCommand(
            async () =>
            {
                try
                {
                    if (_navigating) return;
                    _navigating = true;
                        await this.NavigationService.NavigateToGenericOperation(Constants.SEND_MONEY_SCREEN);
                    _navigating = false; 
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {

                }
            }));
            }
        }

        private RelayCommand _goToPay;
        public RelayCommand GoToPay
        {
            get
            {
                return _goToPay ?? (_goToPay = new RelayCommand(
            async () =>
            {
                try
                {
                    if (_navigating) return;
                    _navigating = true;
                        await this.NavigationService.NavigateToGenericOperation(Constants.PAY_SCREEN);
                    _navigating = false;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {

                }
            }));
            }
        }
    }
}